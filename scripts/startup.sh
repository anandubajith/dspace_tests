#!/bin/sh
echo "SETUP DSPACE"
# downloads and setups up DSpace

#Baixo i Compilo el DSpace
tar -xvf /tmp/dspace.tar.gz --strip-components=1 -C /projects/src/
cp /tmp/local.cfg /projects/src/dspace/config/

# Set variables, and defaults
# POSTGRES_DB_HOST=${POSTGRES_DB_HOST:-localhost}
# POSTGRES_DB_PORT=${POSTGRES_DB_PORT:-5432}
# POSTGRES_USER=${POSTGRES_USER:-dspace}
# POSTGRES_PASSWORD=${POSTGRES_PASSWORD:-dspace}
# POSTGRES_SCHEMA=${POSTGRES_SCHEMA:-dspace}
# LOCAL_CFG=/projects/src/dspace/config/local.cfg

# echo "variables db:host = ${POSTGRES_DB_HOST} db:port = ${POSTGRES_DB_PORT}"
# sed -i "s#db.url = jdbc:postgresql://localhost:5432/dspace#db.url = jdbc:postgresql://${POSTGRES_DB_HOST}:${POSTGRES_DB_PORT}/${POSTGRES_SCHEMA}#" ${LOCAL_CFG}
# sed -i "s#db.username = dspace#db.username = ${POSTGRES_USER}#" ${LOCAL_CFG}
# sed -i "s#db.password = dspace#db.password = ${POSTGRES_PASSWORD}#" ${LOCAL_CFG}
# echo "Dspace configuration changed"

if [ -d "/dspace/webapps" ]; then
  echo "dspace update"
  cd /projects/src/ && mvn package && cd dspace/target/dspace-installer && ant update
else
  sleep 5s
  echo "fresh_install"
  cd /projects/src/ && mvn package && cd dspace/target/dspace-installer && ant fresh_install
fi

if [ -z "$ADMIN_EMAIL" ]; then
  echo "Admin email must be specied"
  exit 1
else
  echo "Creating admin user $ADMIN_EMAIL $ADMIN_PASSWD"
  chmod +x /dspace/bin/dspace
  /dspace/bin/dspace create-administrator -e ${ADMIN_EMAIL} -f DSpace -l Admin -p ${ADMIN_PASSWD} -c en
fi

if grep -q "xmlui" /usr/local/tomcat/conf/server.xml; then
  echo "configuracions fetes"
else
  a=$(cat /usr/local/tomcat/conf/server.xml | grep -n "</Host>" | cut -d : -f 1)
  sed -i "$((a - 1))r /tmp/dspace_tomcat8.conf" /usr/local/tomcat/conf/server.xml
  sleep 2s
fi

/usr/local/tomcat/bin/catalina.sh run
