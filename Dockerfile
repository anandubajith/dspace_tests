FROM openjdk:8
MAINTAINER Anandu <mail@anandu.net>

# Environment variables
ENV TOMCAT_TGZ_URL=https://mirrors.estointernet.in/apache/tomcat/tomcat-8/v8.5.61/bin/apache-tomcat-8.5.61.tar.gz
ENV MAVEN_TGZ_URL=http://apache.mirror.iweb.ca/maven/maven-3/3.3.9/binaries/apache-maven-3.3.9-bin.tar.gz
ENV DSPACE_TGZ_URL=https://github.com/DSpace/DSpace/archive/dspace-6.3.tar.gz
ENV CATALINA_HOME=/usr/local/tomcat
ENV DSPACE_HOME=/dspace
# todo: make sure this works
ENV PATH=$CATALINA_HOME/bin:$DSPACE_HOME/bin:$PATH

# Install runtime and dependencies
RUN apt-get update && apt-get install -y git vim ant postgresql-client

# Making folders and downloads
RUN mkdir -p /dspace \
    && mkdir -p /projects && mkdir -p /projects/src && mkdir -p /projects/src/dspace \
    && mkdir -p maven dspace "$CATALINA_HOME" \
    && curl -fSL "$TOMCAT_TGZ_URL" -o /tmp/tomcat.tar.gz \
    && curl -fSL "$MAVEN_TGZ_URL" -o /tmp/maven.tar.gz \
    && tar -xvf /tmp/tomcat.tar.gz --strip-components=1 -C "$CATALINA_HOME" \
    && tar -xvf /tmp/maven.tar.gz --strip-components=1  -C maven     \
    && ln -s /maven/bin/mvn /usr/bin/mvn \
    && curl -fSL "$DSPACE_TGZ_URL" -o /tmp/dspace.tar.gz


COPY local.cfg /tmp/
COPY scripts/startup.sh /tmp/
COPY dspace_tomcat8.conf /tmp/dspace_tomcat8.conf

RUN chmod +x /tmp/startup.sh

# Start Tomcat
EXPOSE 8080


ENTRYPOINT ["/tmp/startup.sh"]
